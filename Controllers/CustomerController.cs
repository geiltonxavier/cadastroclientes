namespace CadastroClientes.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using DataProvider;
    using Models;

    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        private ICustomerDataProvider CustomerDataProvider;

        public CustomerController(ICustomerDataProvider CustomerDataProvider)
        {
            this.CustomerDataProvider = CustomerDataProvider;
        }

        [HttpGet]
        public async Task> Get()
        {
            return await this.CustomerDataProvider.GetCustomers();
        }

        [HttpGet("{id}")]
        public async Task Get(int id)
        {
            return await this.CustomerDataProvider.GetCustomer(id);
        }

        [HttpPost]
        public async Task Post([FromBody]Customer Customer)
        {
            await this.CustomerDataProvider.AddCustomer(Customer);
        }

        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody]Customer Customer)
        {
            await this.CustomerDataProvider.UpdateCustomer(Customer);
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await this.CustomerDataProvider.DeleteCustomer(id);
        }
    }
}
