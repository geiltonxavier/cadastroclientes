namespace CadastroClientes.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using DataProvider;
    using Models;

    [Route("api/[controller]")]
    public class KindController : Controller
    {
        private IKindDataProvider kindDataProvider;

        public KindController(IKindDataProvider kindDataProvider)
        {
            this.kindDataProvider = kindDataProvider;
        }

        [HttpGet]
        public async Task<IEnumerable<Kind>> Get()
        {
            return await this.kindDataProvider.GetCategories();
        }

        [HttpGet("{id}")]
        public async Task<Kind> Get(int id)
        {
            return await this.kindDataProvider.GetKind(id);
        }

        [HttpPost]
        public async Task Post([FromBody]Kind value)
        {
            await this.kindDataProvider.AddKind(value);
        }

        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody]Kind value)
        {
            await this.kindDataProvider.UpdateKind(value);
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await this.kindDataProvider.DeleteKind(id);
        }
    }
}
