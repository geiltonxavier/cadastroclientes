# ASP.NET Core, Angular 4 & TypeScript

### Frameworks - Tools - Libraries

*   ASP.NET Core
*   Dapper
*   Angular 4
*   Typescript
*   Bootstrap 3
*   Gulp
*   Webpack

### Installation instructions

1.  Install ASP.NET Core according to your development environment from [here](https://www.microsoft.com/net/core).
2.  Install **NPM** by installing [Node.js](https://nodejs.org/en/).
3.  Install Webpack, Gulp, Typescript, following commands on the console/terminal:
    *   npm install -g webpack
    *   npm install -g gulp
    *   npm install -g typescript
    *   npm install