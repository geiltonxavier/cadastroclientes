namespace CadastroClientes.Models
{
    public class Kind
    {
        public int KindId { get; set; }
        public string KindName { get; set; }
    }
}