export interface IKind {
    kindId: number;
    kindName: string;
}