export interface ICustomer {
    customerId: number;
    name: string;
    email: string;
    phone: string;
    kindId: number;
    kindName: string;
}