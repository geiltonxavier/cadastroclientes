import { Component, OnInit, OnDestroy } from "@angular/core";
import { ICustomer } from "./Models/customer.interface";
import { IKind } from "./Models/kind.interface";
import { AppService } from "./app.service";
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
    selector: 'my-app',
    template: require('./app.html')
})

export class AppComponent {
    customers: ICustomer[] = [];
    kinds: IKind[] = [];
    formLabel: string;
    isEditMode = false;
    form: FormGroup;
    customer: ICustomer = <ICustomer>{};

    constructor(private appService: AppService, private formBuilder: FormBuilder) {
        this.form = formBuilder.group({
            "name": ["", Validators.required],
            "email": ["", [Validators.required, Validators.pattern("^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+$")]],
            "phone": ["", [Validators.required, Validators.pattern("^[0-9]*$")]],
            "kind": ["", Validators.required]
        });

        this.formLabel = "Add Customer";
    }

    ngOnInit() {
        this.getCustomers();

        this.appService.getCategories().subscribe(kinds => {
            this.kinds = kinds;
        });

        this.appService.getKinds().subscribe(kinds => {
            this.kinds = kinds;
        });
    }

    onSubmit() {
        this.customer.name = this.form.controls['name'].value;
        this.customer.email = this.form.controls['email'].value;
        this.customer.phone = this.form.controls['phone'].value;
        this.customer.kindId = this.form.controls['kind'].value;
        if (this.isEditMode) {
            this.appService.editCustomer(this.customer).subscribe(response => {
                this.getCustomers();
                this.form.reset();
            });
        } else {
            this.appService.addCustomer(this.customer).subscribe(response => {
                this.getCustomers();
                this.form.reset();
            });
        }   
    }

    cancel() {
        this.formLabel = "Add Customer";
        this.isEditMode = false;
        this.customer = <ICustomer>{};
        this.form.get("name").setValue('');
        this.form.get("email").setValue('');
        this.form.get('phone').setValue('');
        this.form.get('kind').setValue(0);
       
    }

    edit(customer: ICustomer) {
        this.formLabel = "Edit Customer";
        this.isEditMode = true;
        this.customer = customer;
        this.form.get("name").setValue(customer.name);
        this.form.get("email").setValue(customer.email); 
        this.form.get('phone').setValue(customer.phone); 
        this.form.get('kind').setValue(customer.kindId); 
        
    }

    delete(customer: ICustomer) {
        if (confirm("Are you sure want to delete this?")) {
            this.appService.deleteCustomer(customer.customerId).subscribe(response => {
                this.getCustomers();
                this.form.reset();
            });
        }
    }

    private getCustomers() {
        this.appService.getCustomers().subscribe(customers => {
            this.customers = customers;
        });
    }
}