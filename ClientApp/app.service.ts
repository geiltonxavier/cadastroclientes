import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { ICustomer } from "./Models/customer.interface";
import { IKind } from "./Models/kind.interface";


@Injectable()
export class AppService {
    constructor(private http: Http) { }

    getCustomers() {
        return this.http.get("/api/customer").map(data => <ICustomer[]>data.json());
    }

    addCustomer(customer: ICustomer) {
        return this.http.post("/api/customer", customer);
    }

    editCustomer(customer: ICustomer) {
        return this.http.put(`/api/customer/${customer.customerId}`, customer);
    }

    deleteCustomer(customerId: number) {
        return this.http.delete(`/api/customer/${customerId}`);
    }

    getKinds() {
        return this.http.get("/api/kind").map(data => <IKind[]>data.json());
    }
    
}