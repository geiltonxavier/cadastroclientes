namespace CadastroClientes.DataProvider
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Models;
    using Dapper;
    using System.Data;

    public class CustomerDataProvider : ICustomerDataProvider
    {
        private readonly string connectionString = "Server=localhost;Database=Clientes;Trusted_Connection=True;";

        private SqlConnection sqlConnection;

        public async Task AddCustomer(Customer Customer)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                await sqlConnection.OpenAsync();
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@Name", Customer.Name);
                dynamicParameters.Add("@Email", Customer.Email);
                dynamicParameters.Add("@Phone", Customer.Phone);
                dynamicParameters.Add("@KindId", Customer.KindId);
                await sqlConnection.ExecuteAsync(
                    "AddCustomer",
                    dynamicParameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public async Task DeleteCustomer(int id)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                await sqlConnection.OpenAsync();
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@CustomerId", id);
                await sqlConnection.ExecuteAsync(
                    "DeleteCustomer",
                    dynamicParameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<Customer> GetCustomer(int id)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                await sqlConnection.OpenAsync();
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@id", id);
                return await sqlConnection.QuerySingleOrDefaultAsync<Customer>(
                    "GetCustomer", 
                    dynamicParameters, 
                    commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Customer>> GetCustomers()
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                await sqlConnection.OpenAsync();
                return await sqlConnection.QueryAsync<Customer>(
                    "GetCustomers",
                    null,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public async Task UpdateCustomer(Customer Customer)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                await sqlConnection.OpenAsync();
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@CustomerId", Customer.CustomerId);
                dynamicParameters.Add("@name", Customer.Name);
                dynamicParameters.Add("@Email", Customer.Email);
                dynamicParameters.Add("@Phone", Customer.Phone);
                dynamicParameters.Add("@KindId", Customer.KindId);
                await sqlConnection.ExecuteAsync(
                    "UpdateCustomer",
                    dynamicParameters,
                    commandType: CommandType.StoredProcedure);
            }
        }
    }
}
