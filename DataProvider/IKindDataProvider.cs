namespace CadastroClientes.DataProvider
{
    using Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IKindDataProvider
    {
        Task<IEnumerable<Kind>> GetKinds();

        Task<Kind> GetKind(int id);

        Task AddKind(Kind kind);

        Task UpdateKind(Kind kind);

        Task DeleteKind(int id);
    }
}
