
namespace CadastroClientes.DataProvider
{
    using Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface ICustomerDataProvider
    {
        Task<IEnumerable<Customer>> GetCustomers();

        Task<Customer> GetCustomer(int id);

        Task AddCustomer(Customer Customer);

        Task UpdateCustomer(Customer Customer);

        Task DeleteCustomer(int id);
    }
}
