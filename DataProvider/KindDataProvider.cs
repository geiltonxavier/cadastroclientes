namespace CadastroClientes.DataProvider
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Models;
    using Dapper;

    public class KindDataProvider : IKindDataProvider
    {
        private readonly string connectionString = "Server=localhost;Database=Clientes;Trusted_Connection=True;";

        private SqlConnection sqlConnection;

        public async Task AddKind(Kind kind)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                await sqlConnection.OpenAsync();
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@KindId", kind.KindId);
                dynamicParameters.Add("@Name", kind.KindName);
                await sqlConnection.ExecuteAsync(
                    "AddKind",
                    dynamicParameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public async Task DeleteKind(int id)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                await sqlConnection.OpenAsync();
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@id", id);
                await sqlConnection.ExecuteAsync(
                    "DeleteKind",
                    dynamicParameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<IEnumerable<Kind>> GetKinds()
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                await sqlConnection.OpenAsync();
                return await sqlConnection.QueryAsync<Kind>(
                    "GetKinds",
                    null,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public async Task<Kind> GetKind(int id)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                await sqlConnection.OpenAsync();
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@id", id);
                return await sqlConnection.QuerySingleOrDefaultAsync<Kind>(
                    "GetKind",
                    dynamicParameters,
                    commandType: CommandType.StoredProcedure);
            }
        }

        public async Task UpdateKind(Kind kind)
        {
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                await sqlConnection.OpenAsync();
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@KindId", kind.KindId);
                dynamicParameters.Add("@Name", kind.KindName);
                await sqlConnection.ExecuteAsync(
                    "UpdateKind",
                    dynamicParameters,
                    commandType: CommandType.StoredProcedure);
            }
        }
    }
}
