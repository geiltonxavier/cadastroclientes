﻿CREATE TABLE [dbo].[Customer] (
    [CustomerId]       INT           IDENTITY (1, 1) NOT NULL,
    [Name]            NVARCHAR (50) NULL,
    [Email]        NVARCHAR (50) NULL,
    [Phone]           NVARCHAR (22) NULL,
    [KindId]      INT           NULL,
    [LastUpdatedDate] DATETIME      NULL,
    CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED ([CustomerId] ASC),
    CONSTRAINT [FK_Customer_Kind] FOREIGN KEY ([KindId]) REFERENCES [dbo].[Kind] ([KindId]),
    
);

