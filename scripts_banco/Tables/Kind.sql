﻿CREATE TABLE [dbo].[Kind] (
    [KindId]   INT           NOT NULL,
    [KindName] NVARCHAR (50) NULL,
    CONSTRAINT [PK_Kind] PRIMARY KEY CLUSTERED ([KindId] ASC)
);

