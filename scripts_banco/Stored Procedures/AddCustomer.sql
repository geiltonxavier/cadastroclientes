﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddCustomer] 
	@name NVARCHAR(50),
	@email NVARCHAR(50),
	@phone NVARCHAR(22),
	@kindId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.Customer VALUES (@name, @email, @phone, @kindId, GETDATE())
END
