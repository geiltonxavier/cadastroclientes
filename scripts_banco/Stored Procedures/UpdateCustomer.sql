﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE UpdateCustomer 
	@customerId INT,
	@name NVARCHAR(50),
	@email NVARCHAR(50),
	@phone NVARCHAR(22),
	@kindId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.Customer SET Name = @name, Email = @email, Phone = @phone, KindId = @kindId, LASTUPDATEDDATE = GETDATE() WHERE CustomerId = @customerId 
END
